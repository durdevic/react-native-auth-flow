This is a branch for 64 app, the latest changes are on **style** and **lastzero** branch

# react-native-auth-flow
Proper Auth flow structure with React Navigation v2 https://reactnavigation.org/docs/en/auth-flow.html

Easy to separate and maintain rest of the application from auth screens

Cheers! 👍