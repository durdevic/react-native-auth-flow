import { StackNavigator, SwitchNavigator } from 'react-navigation'; // Version can be specified in package.json

// import { HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen } from './src/screens';
import HomeScreen from './src/screens/HomeScreen';
import OtherScreen from './src/screens/OtherScreen';
import SignInScreen from './src/screens/AuthScreens/SignInScreen';
import AuthLoadingScreen from './src/screens/AuthScreens/AuthLoadingScreen';

console.disableYellowBox = true;

const AppStack = StackNavigator({ Home: HomeScreen, Other: OtherScreen });
const AuthStack = StackNavigator({ SignIn: SignInScreen });

export default SwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);
